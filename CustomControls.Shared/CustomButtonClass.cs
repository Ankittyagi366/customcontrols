﻿using System;
#if __IOS__
using UIKit;
using Xamarin.Forms.Platform.iOS;
#endif

#if __ANDROID__
using Xamarin.Forms.Platform.Android;
using Android.App;

using Android.Content;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
#endif

using Xamarin.Forms;

using CustomControls.Interface;
using CustomControls.Shared;

[assembly: Dependency(typeof(CustomButtonClass))]
namespace CustomControls.Shared
{
    public class CustomButtonClass : ICustomButtonInterface
    {
        public CustomButtonClass()
        {

        }
        /// <summary>
        /// Variable Declaration
        /// </summary>
        /// <returns>The button.</returns>
        /// <param name="text">Text.</param>
        /// <param name="color">Color.</param>
#if __IOS__

        UIButton button = null;
        UILongPressGestureRecognizer longButtonPress = null;
        object sender = null;

#endif
#if __ANDROID__

        public Android.Widget.Button Mybutton=null;

#endif

        public event EventHandler OnButtonClicked;
        public event EventHandler OnLongButtonPressed;

        public Xamarin.Forms.View CustomButton(string text, Color color)
        {
#if __IOS__

           button = new UIButton(UIButtonType.RoundedRect);
            button.UserInteractionEnabled = true;
            longButtonPress = new UILongPressGestureRecognizer(LongPress);
           
            button.BackgroundColor = UIColor.Magenta;
            button.AddGestureRecognizer(longButtonPress);
            button.TouchUpInside += Button_TouchUpInside;
            button.Layer.CornerRadius = 10;
            button.SetTitle(text, UIControlState.Normal);
            button.SetTitleColor(UIColor.Cyan, UIControlState.Normal);

            return button.ToView();
#endif
#if __ANDROID__

            Mybutton = new Android.Widget.Button(Forms.Context);
            Mybutton.Text = text;
            Mybutton.SetTextColor(Color.White.ToAndroid());
            Mybutton.SetBackgroundColor(color.ToAndroid());

            Mybutton.LongClick += LongClick_Android;
            Mybutton.Click += Button_Clicked;


            return Mybutton.ToView();
#endif

        }
        #region EventsonButton
        // Clicked Events for Button

#if __IOS__

        private void LongPress()
        {
            
                sender = new object();
             
                OnLongButtonPressed?.Invoke(sender, new EventArgs());
      }
#endif


#if __IOS__
        void Button_TouchUpInside(object sender, EventArgs e)
        {
        OnButtonClicked?.Invoke(sender,e);
        }
#endif
#if __ANDROID__

        void Button_Clicked(object sender, EventArgs e)
        {

            OnButtonClicked?.Invoke(sender, e);
        }

        private void LongClick_Android(object sender, Android.Views.View.LongClickEventArgs e)
        {
            OnLongButtonPressed?.Invoke(sender, e);
        }
#endif

#endregion
    }

}
