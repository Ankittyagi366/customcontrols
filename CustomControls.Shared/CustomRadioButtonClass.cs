﻿using System;
using CustomControls.Shared;
using CustomControls.Interface;

#if __IOS__

using UIKit;

#endif
#if __ANDROID__
using Android.App;
using Android.Widget;
using Android.Content.PM;
using Xamarin.Forms.Platform.Android;

#endif
using Xamarin.Forms;


[assembly: Dependency(typeof(CustomRadioButtonClass))]
namespace CustomControls.Shared
{
    public class CustomRadioButtonClass : ICustomRadioButtonInterface
    {
        /// <summary>
        /// Events
        /// </summary>

        public event EventHandler<string> MaleClick;
        public event EventHandler<string> FemaleClick;

        /// <summary>
        ///  Control Declaration
        /// </summary>
        /// 

#if __ANDROID__
        RadioGroup radioButtonGroup = null;
        RadioButton radioButtonMale = null;
        RadioButton radioButtonFemale = null;
        IAsyncResult Maleresult = null;
        IAsyncResult Femaleresult = null;

#endif
#if __IOS__

#endif

        public CustomRadioButtonClass()
        {
            
        }



        public Xamarin.Forms.View GetRadioButton()
        {
#if __IOS__

#endif
#if __ANDROID__

            radioButtonGroup = new RadioGroup(Forms.Context);
            radioButtonGroup.Orientation = Orientation.Horizontal;
            radioButtonMale = new RadioButton(Forms.Context);
            radioButtonMale.Text = "Male";
            radioButtonMale.Click += RadioButtonMale_Selected;


            radioButtonFemale = new RadioButton(Forms.Context);

            radioButtonFemale.Text = "Female";
            radioButtonFemale.Click += RadioButtonFemale_Selected;
            radioButtonGroup.AddView(radioButtonMale);
            radioButtonGroup.AddView(radioButtonFemale);

            return radioButtonGroup.ToView();

#endif
        }
        #region Events on Radio Button 

#if __ANDROID__
        private void RadioButtonFemale_Selected(object sender, EventArgs e)
        {
            
            FemaleClick?.Invoke(sender,"Female");
        }

        private void RadioButtonMale_Selected(object sender, EventArgs e)
        {

            MaleClick?.Invoke(sender, "Male");
        }
#endif
        #endregion

    }
}
