﻿using System;
using Xamarin.Forms;
using CustomControls.Interface;
namespace CustomControls
{
    public partial class CustomControlsPage : ContentPage
    {
        ICustomButtonInterface myButton = null;
        ICustomRadioButtonInterface myRadioButtons = null;
        public CustomControlsPage()
        {
            
            InitializeComponent();
           
           

            LoadButton();
            LoadRadioButton();
        }

        private void btnClicked(object sender, EventArgs e)
        {
            
          
                DisplayAlert("Long Button Pressed", "Long Button Pressed", "OK");
             
        }

        private void LoadButton()
        {
            myButton = DependencyService.Get<ICustomButtonInterface>();
            myButton.OnButtonClicked += (sender, e) => {

                DisplayAlert("Button Clicked", "Button Clicked", "OK");
            };
            myButton.OnLongButtonPressed += btnClicked;
            stackMain.Children.Add(DependencyService.Get<ICustomButtonInterface>().CustomButton("Login", Color.Black));

        }

        private void LoadRadioButton()
        {
            myRadioButtons = DependencyService.Get<ICustomRadioButtonInterface>();
            myRadioButtons.MaleClick += (sender, e) => { 
            
                DisplayAlert("Radio Button Selected",e.ToString(),"OK");
            
            };
            myRadioButtons.FemaleClick += (sender, e) => {

                DisplayAlert("Radio Button Selected", e.ToString(), "OK");

            };
            stackMain.Children.Add(DependencyService.Get<ICustomRadioButtonInterface>().GetRadioButton());

        }
    }
}
