﻿using System;

using Xamarin.Forms;

namespace CustomControls.Interface
{
     public interface ICustomButtonInterface 
    {
        Xamarin.Forms.View CustomButton(string text, Color color);
         event EventHandler OnButtonClicked;
        event EventHandler OnLongButtonPressed;
    }
}

