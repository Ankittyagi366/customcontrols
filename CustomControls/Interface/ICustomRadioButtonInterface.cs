﻿using System;
namespace CustomControls.Interface
{
    public interface ICustomRadioButtonInterface
    {
        Xamarin.Forms.View GetRadioButton();
        event EventHandler<string> MaleClick;
        event EventHandler<string> FemaleClick;

       
    }
}
